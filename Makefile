OPT=-O2
CC=gcc

help:
	@echo "Sysop project 2014 by Martin Brugnara M:157791, Alex De Biasio M:158795"
	@echo "Client-Server CODER - Progetto 1"
	@echo "Valid targets:"
	@echo "\tbin           compile the sources"
	@echo "\tassets        does nothing"
	@echo "\ttest          test the input"
	@echo "\tinstall"
	@echo "\tclean"

all: build

install: build
	cp bin/server /usr/bin/
	cp bin/client /usr/bin/

build: clean bin_dir server client

assets: help
	@echo "\nI file necessari per i test sono gia presenti in assets (non vengono generati dinamicamente)\n"

server: bin_dir srv_core.o conn.o shell.o cad.o mem.o utils.o
	$(CC) $(OPT) -o bin/server bin/cad.o bin/srv_core.o bin/conn.o bin/mem.o bin/utils.o bin/shell.o -lpthread -lm src/server.c
	chmod +x bin/server

client: bin_dir conn.o utils.o
	$(CC) $(OPT)  -o bin/client bin/conn.o bin/utils.o src/client.c -lm -lpthread
	chmod +x bin/client

srv_core.o: bin_dir conn.o cad.o
	$(CC) $(OPT) -c -o bin/srv_core.o src/srv_core.c 

conn.o: bin_dir utils.o
	$(CC) $(OPT) -c -o bin/conn.o src/conn.c

shell.o: bin_dir mem.o utils.o cad.o
	$(CC) $(OPT) -c -o bin/shell.o  src/shell.c

utils.o: bin_dir
	$(CC) $(OPT) -c -o bin/utils.o src/utils.c

cad.o: bin_dir mem.o
	$(CC) $(OPT) -c -o bin/cad.o src/cad.c

mem.o: bin_dir
	$(CC) $(OPT) -c -o bin/mem.o src/mem.c

mem_test: bin_dir mem.o
	$(CC) $(OPT) -o bin/test_mem bin/mem.o tests/mem_test.c

cad_test: bin_dir cad.o mem.o
	$(CC) $(OPT) -o bin/test_cad bin/cad.o bin/mem.o tests/cad_test.c

test: build cad_test mem_test
	# test encoding - decoding alone
	./bin/test_cad
	
	# test memory management function
	./bin/test_mem
	
	# test complete connection
	cp ./tests/test_single.sh ./bin/
	chmod +x ./bin/test_single.sh
	./bin/test_single.sh
	rm ./bin/test_single.sh

	# test completed
	@echo "Test completed"
	@echo ""

unistall:
	rm -rf /usr/bin/client /usr/bin/server

bin_dir:
	mkdir -p ./bin

clean:
	mkdir -p ./bin
	rm -rf ./bin

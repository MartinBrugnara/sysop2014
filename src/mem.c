/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#include "mem.h"
#include <stdlib.h>
#include <stdio.h>
#include "utils.h"
#include <string.h>


typedef struct node{
    const char *msg;
    struct node *next;
}node;

typedef node list;

static list *l=NULL;
static int id=0;

// copy a string (allocating the required buffer)
// return the copy of the string
char * xstrcpy(const char * m) {
    int len = strlen(m);
    char * ret = malloc(len + 1);
    strcpy(ret, m);
    return ret;
}

// store a message
int store(const char * msg){
    node * n ;
    if((n=malloc(sizeof(struct node)))==NULL)
        return -1;
    n->msg = xstrcpy(msg);
    n->next=l;
    l=n;
    id++;
    return 0;
}

// retrieve a message by id
//
// char **str: will be the request message if found 
// return 0 on success -1 on error
int get(int i, char **str){
    if(i>=id)
        return -1;
    node *p;

    int k;
    for(p=l, k=id-1; k > i && p != NULL; p=p->next,k--)
        ;

    *str= (char*)xstrcpy(p->msg);
    return 0;
}

// print the history to screen
void print_list( void){
    node *p;
    int i;

    for(p=l,i=id-1; p!=NULL; p=p->next,i--)
        fprintf(stdout,"%04d:    %-70.70s\n",i,p->msg);
    return;
}

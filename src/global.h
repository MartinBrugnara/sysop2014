/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#ifndef GLOBAL_H
#define GLOBAL_H

struct conf_t {
    char * srv_name; // -n
    int max_mex_len; // -m
    int min_key_len; // -s
    int max_key_len; // -l
};
typedef struct conf_t conf_t;

// GLOBAL VARS
extern conf_t * CONF;
extern const int COM_ERROR;
extern FILE *logger;
#endif

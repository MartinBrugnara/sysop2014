/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#include <unistd.h> // read
#include <stdio.h> // stdout, logger
#include <stdlib.h> // free

#include "global.h"
#include "conn.h" // read_msg, write_msg
#include "cad.h" // encode, decode
#include "srv_core.h" // worker


/*
 * This is the func that will process the remote client request
 * in int: cli->srv fifo fd
 * out int: srv->cli fifo fd
 */
void worker(int cli_to_srv, int srv_to_cli, pid_t client_pid) {
    fprintf(logger, "[II] in worker 4 %d\n", client_pid);

    /* NOTE: char * 
     * should always be init to NULL
     * cause free(NULL) is safe, free(garbage) is a "Russian roulette".
     */

    cmd_t cmd;
    char * key = NULL;
    char * txt = NULL;
    char * res = NULL;

    if (read(cli_to_srv, &cmd, sizeof(cmd)) < 0) {
        write_error(srv_to_cli, "Reading cmd");
        fprintf(logger, "[EE] %d reading cmd from client\n", client_pid); 
        goto clean;
    } else {
        fprintf(logger, "[II] %d readed cmd from client\n", client_pid); 
    }

    // check is cmd is allowed
    if (cmd != ENC && cmd != DEC) {
        write_error(srv_to_cli, "CMD non allowed");
        fprintf(logger, "[EE] %d cmd not allowed\n", client_pid); 
        goto clean;
    } else {
        fprintf(logger, "[II] %d cmd ok\n", client_pid); 
    }

    int rc;
    if ((rc = read_msg(cli_to_srv, &key, CONF->max_key_len)) == -3) {
        write_error(srv_to_cli, "key is too long");
        fprintf(logger, "[EE] %d reading key from client (too long)\n", client_pid); 
        goto clean;
    } 
    
    if (rc < CONF->min_key_len) {
        write_error(srv_to_cli, "key is too short");
        fprintf(logger, "[EE] %d reading key from client (too short)\n", client_pid); 
        goto clean;
    } 
    
    if (rc < 0) {
        write_error(srv_to_cli, "reading key");
        fprintf(logger, "[EE] %d reading key from client\n", client_pid); 
        goto clean;
    } 

    fprintf(logger, "[II] %d key readed\n", client_pid);
    
    int len; // rc yet defined up
    if ((rc = (len = read_msg(cli_to_srv, &txt, CONF->max_mex_len))) == -3) {
        write_error(srv_to_cli, "msg too long");
        fprintf(logger, "[EE] %d reading txt from client (msg too long)\n", client_pid); 
        goto clean;
    } 
    
    if (rc < 0) {
        write_error(srv_to_cli, "reading txt");
        fprintf(logger, "[EE] %d reading txt from client\n", client_pid); 
        goto clean;
    } 

    fprintf(logger, "[II] %d msg readed\n", client_pid);

    switch (cmd) {
    case ENC:
        res = encode(txt, key);
        break;
    case DEC:
        res = decode(txt, key);
        break;
    default:
        // this will never happen;
        break;
    }

    if(!write_msg(srv_to_cli, res, len)) {
        write_error(srv_to_cli, "sending result");
        fprintf(logger, "[EE] %d sending result to client\n", client_pid); 
        goto clean;
    }

clean:
    // close fifos fd
    close(cli_to_srv);
    close(srv_to_cli);
    // free mem
    free(key);
    free(txt);
    free(res);
}

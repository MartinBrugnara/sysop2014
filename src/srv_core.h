/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#ifndef SRV_CORE_H
#define SRV_CORE_H

void worker(int in, int out, pid_t client_pid);

#endif

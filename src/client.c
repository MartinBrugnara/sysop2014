/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include "conn.h"

struct params_t {
	char * key;
	char * msg;
	char * srv_name;
	char * input_file_path;
	char * output_file_path;
	cmd_t cmd;
};
typedef struct params_t params_t;

params_t * PARAMS;
FILE * logger;

void parse_opt(const int argc, char ** argv) {
    PARAMS = calloc(1, sizeof(params_t));

    uint8_t p = 0;
    uint8_t u = 0b1;

    char c;
    while ((c=getopt(argc,argv,"n:k:f:m:edo:hv:?")) != -1){		
        switch(c){
            case 'n':			
                PARAMS->srv_name = optarg;					
                p |= u << 0;
                break;
            case 'k':
                PARAMS->key = optarg;
                p |= u << 1;
                break;
            case 'm':
                PARAMS->msg = optarg;
                p |= u << 2;
                break;
            case 'e':
                PARAMS->cmd = ENC;
                p |= u << 3;
                break;
            case 'd':
                PARAMS->cmd = DEC;
                p |= u << 3;
                break;
            case 'f':
                PARAMS->input_file_path = optarg;
                p |= u << 2;
                break;
            case 'o':
                PARAMS->output_file_path = optarg;
                break;
            case 'v': //log in another file
                if((logger=fopen(optarg,"a+"))==NULL){
                    logger=stderr;
                    fprintf(logger, "[EE] opening log file\n");
                }
                break;



            case '?': 
                fprintf(logger,"\t [EE] option -%c requires an argument.\n",c);
                switch(optopt) {
                    case 'n':
                    case 'k':
                    case 'm':
                    case 'e':
                    case 'd':
                    case 'f':
                    case 'o':
                        fprintf(logger,"[EE] option -%c requires an argument.\n", optopt);
                        break;                    
                    default:
                        fprintf(logger, "Unrecognized option %c", c);
                }
                exit(1);

            case 'h':
                fprintf(logger,"Usage:\t%s [-n SERVER_NAME] [-k KEY] [-f INPUT_FILE | -m MESSAGE] [-e | -d] [-o OUTPUT_FILE] [-v LOG_FILE]\n",argv[0]);	
                exit(0);
        }
    }

    if (p !=  0b00001111) {
        fprintf(logger,"[EE] missing argument.\n");
        fprintf(logger,"Usage:\t%s [-n SERVER_NAME] [-k KEY] [-f INPUT_FILE | -m MESSAGE] [-e | -d] [-o OUTPUT_FILE] [-v LOG_FILE]\n",argv[0]);	
        exit (1);
    }
}

int main(int argc, char **argv) {
    
    logger=stderr;

	FILE * input = 0, * output = 0;

	int srv_to_cli, cli_to_srv; // FIFO fd

    size_t file_msg_len;

    char * res = 0;
    // Parse cmd line options
    parse_opt(argc, argv);


    // Open, if req input and output file	
	if (PARAMS->input_file_path) {
		if (!(input = fopen(PARAMS->input_file_path, "r"))) {
			fprintf(logger, "[EE] file not found.\n%s\n",
                    PARAMS->input_file_path);
			return -1;
		}
	}
	if (PARAMS->output_file_path) {
		if (!(output = fopen(PARAMS->output_file_path, "w+"))) {
			fprintf(logger, "\t[EE] file not found.\n" );
			return -1;
		}
	} else {
		output = stdout;
    }

	// Prepare to send req to server
    if (input) {
        fseek (input, 0L, SEEK_END); // go to EOF
        file_msg_len = ftell(input); // get file len

        PARAMS->msg = calloc(1+file_msg_len, sizeof(char));

        fseek (input, 0L, SEEK_SET); // go to file start

        int i = 0;
        while ((PARAMS->msg[i++] = (char)fgetc(input)) != EOF);

        PARAMS->msg[i-1] = '\0';
    }


    // Connect to server
	if (client_connect(PARAMS->srv_name, &srv_to_cli, &cli_to_srv) < 0){
        fprintf(logger,"[EE] connecting with server.\n");
		return -1;
    }
    // Make request
    // Send command
    if (write(cli_to_srv, (void *)&(PARAMS->cmd), sizeof(cmd_t)) <= 0)
        goto error;

    // Send key
    if (!write_msg(cli_to_srv, PARAMS->key, 0)) 
        goto error;

    // Send message
    if (!write_msg(cli_to_srv, PARAMS->msg, 0))
        goto error;

    // Read response
    if (read_msg(srv_to_cli, &res, 0) <= 0) 
        goto error;                       

    // Write response on output
    fprintf(output, "%s\n", res);

    goto cleanup;
error:
    fprintf(logger, "[EE] processing the request\n");
    if (res) // contains server sent error message
        fprintf(logger, "\t%s\n", res);

cleanup:
	
    // Close connections & destroy fifo
    client_close(PARAMS->srv_name, cli_to_srv, srv_to_cli);

    // Close output file
    if (PARAMS->output_file_path)
        fclose(output);

	return 0;
}

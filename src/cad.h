/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#ifndef CAD_H
#define CAD_H
char * encode(const char * msg, const char * key); 
char * decode(const char * encoded_msg, const char * key);
#endif

/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <stdint.h>

#include "conn.h" // server_up
#include "srv_core.h" // worker
#include "shell.h" //shell

#include "global.h"
conf_t * CONF;

FILE *logger=NULL;

// FUNC IMPL;
void popt(const int argc, char ** argv) {
    CONF =(conf_t*) malloc(sizeof(conf_t));
    uint8_t p = 0;
    uint8_t m = 0b1;
    char c;
    while ((c = getopt(argc, argv, "n:m:s:l:v:")) != -1) {
        switch (c) {
            case 'n':
                CONF->srv_name = optarg;
                p |= m<<0;
                break;
            case 'm': //mex length
                CONF->max_mex_len = atoi(optarg);
                p |= m<<1;
                break;
            case 's': //short key
                CONF->min_key_len = atoi(optarg);
                p |= m<<2;
                break;
            case 'l': //log key
                CONF->max_key_len = atoi(optarg);
            p |= m<<3;
            break;
        case 'v': //log in another file
            if((logger=fopen(optarg,"w+"))==NULL){
                logger=stderr;
                fprintf(logger, "[EE] opening log file\n");
            }
            break;

        case '?': 
            switch(optopt) {
                case 'n':
                case 's':
                case 'm':
                case 'l':
                    fprintf(stderr,"Error: option -%c requires an argument.\n", optopt);
                    exit(1);
                    break;                    
                default:
                    fprintf(stderr, "Unrecognized option %c\n", c);
                    exit(1);
            }      
        } 
    }
    if (p != 0b00001111){
        fprintf(stderr, "[EE] missing argument\n");
        exit(1);
    }

    //if -v option is not present, open std file log in /var/log/srvname_encoder.log
    if(logger==NULL){
        char path[128];
        sprintf(path,"/var/log/%s_encoder.log",CONF->srv_name);
        if((logger=fopen(path,"w+"))==NULL)
            logger=stderr;
    }
}

// fuck we haven't got anonimus func (and callback.h is not an opt =( )
// This stuff allow server_up func to be wrapped in order to be 
// ran in a pthread
struct srv_up_w_argv {
char * srv_name;
void (* handler)(int, int, pid_t);
};

void * srv_up_wrapper(void * argv) {
struct srv_up_w_argv * a = (struct srv_up_w_argv *)argv;
server_up(a->srv_name, a->handler);
    return NULL;
}

// MAIN
int main (int argc, char ** argv) {

    popt(argc, argv);

    pthread_t srv;

    struct srv_up_w_argv * a = malloc(sizeof(struct srv_up_w_argv));
    a->srv_name = CONF->srv_name;
    a->handler = &worker;
    if (pthread_create(&srv, NULL, &srv_up_wrapper, a)) {
        fprintf(logger, "[EE] staring srv thread\n");
    } else {
        fprintf(logger, "[II] srv thread started\n");
    }

    shell();
}

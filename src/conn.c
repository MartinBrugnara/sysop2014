/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#include <string.h>

#include <pthread.h>
#include "utils.h"
#include "conn.h"

//logger file desc
extern FILE *logger;

// ERROR code to return to client when something goes wrong
const int COM_ERROR = -666;
const char * LOC = "/tmp/";
const char * EXT = ".sock";

// common struct
struct handler_argv {
    pid_t client_pid;
    void (* handler)(int, int, pid_t);
    const char * srv_name;
};

// common func
char * name_cli_to_srv(const char * srv_name, const pid_t pid) {
    char * addr = calloc((
                strlen(LOC) + 
                strlen(srv_name) + 
                intlen(pid) + 
                strlen(EXT) +
                1), sizeof(char)); 
    sprintf(addr, "%s%s%d%s", LOC, srv_name, pid, EXT);
    return addr;
}

char * name_srv_to_cli(const pid_t pid) {
    char * addr = calloc((
                strlen(LOC) + 
                intlen(pid) +
                strlen(EXT) +
                1), sizeof(char)); 
    sprintf(addr, "%s%d%s", LOC, pid, EXT);
    return addr;
}

// SRV
void * handle_client (void * hargv_v) {
    struct handler_argv * hargv = hargv_v;

    char * cli_to_srv_addr, * srv_to_cli_addr;
    int cli_to_srv, srv_to_cli; // fd

    // client -> srv
    // client_pid + .conn
    cli_to_srv_addr = name_cli_to_srv(hargv->srv_name, hargv->client_pid);
    if ((cli_to_srv = open(cli_to_srv_addr, O_RDWR)) < 0) {
        fprintf(logger, "[EE] %d binding cli -> srv pipe %s\n", 
                hargv->client_pid,
                cli_to_srv_addr);
        free(cli_to_srv_addr);
        return NULL;
    } else {
        fprintf(logger, "[II] %d binded client->srv pipe %s\n",  
                hargv->client_pid,
                cli_to_srv_addr);
    }
    free(cli_to_srv_addr);

    // srv -> client 
    // srv_name + client_pid + .conn
    srv_to_cli_addr = name_srv_to_cli(hargv->client_pid);
    if ((srv_to_cli = open(srv_to_cli_addr, O_RDWR)) < 0) {
        fprintf(logger, "[EE] %d binding srv -> client pipe %s\n", 
                hargv->client_pid,
                srv_to_cli_addr);
        free(srv_to_cli_addr);
        return NULL;
    } else {
        fprintf(logger, "[II] %d binded srv -> client pipe %s\n", 
                hargv->client_pid,
                srv_to_cli_addr);
    }
    free(srv_to_cli_addr);

    const int OK = 200;
    if (write(srv_to_cli, (void*)&OK, sizeof(int)) < 0) {
        fprintf(logger, "[EE] %d sending 200 OK to client\n", hargv->client_pid);
        return NULL;
    } else {
        fprintf(logger, "[II] %d sended 200 OK to client\n", hargv->client_pid);
    }

    // call handler passing [in, out]
    void (* h)(int, int, pid_t) = hargv->handler;
    pid_t cpid = hargv->client_pid;

    free(hargv);

    h(cli_to_srv, srv_to_cli, cpid);

    return 0;
}

// prepare server background task (client handling)
void server_up(const char * server_name, void (* handler)(int, int, pid_t)) {
    // 0. Set up master pipe 
    char * srv_addr = calloc((
                strlen(LOC) +
                strlen(server_name) +
                strlen(EXT) + 
                1), sizeof(char));
    int srv_fd = 0;

    sprintf(srv_addr, "%s%s%s", LOC, server_name, EXT);

    unlink(srv_addr);
    if (mkfifo(srv_addr, 0666) == -1) {
        fprintf(logger, "[EE] Error creating server main fifo");
        goto error;
    } else {
        fprintf(logger, "[II] Master fifo is ready\n");
    }

    if ((srv_fd = open(srv_addr, O_RDWR)) < 0) {
        fprintf(logger, "[EE] Error opening server main fifo in read mode");
        goto error;
    } else {
        fprintf(logger, "[II] Master fifo is listening @ %s\n", srv_addr);
    }

    // Accept clients
    pid_t client_pid;
    struct handler_argv * hargv;
    for (;;) {
        if (read(srv_fd, (void*)&client_pid, sizeof(pid_t)) > 0) {
            fprintf(logger, "[II] got new client %d\n", client_pid);
            if (client_pid <= 1) // init or kernel
                continue;

            /* Handle the connection in a separate thread. */
            int t_err;
            pthread_t t_id;

            hargv = malloc(sizeof(struct handler_argv));
            hargv->client_pid = client_pid;
            hargv->handler = handler;
            hargv->srv_name = server_name;

            if ((t_err = pthread_create(&t_id, NULL, &handle_client, (void*)hargv)) != 0) {
                fprintf(logger, "[EE] pthread: dispatching new req %d\n", t_err);
            } else {
                fprintf(logger, "[II] handling client %d\n", client_pid);
            }
        } else {
            fprintf(logger, "[FF] accepting new client %d\n", errno);
            goto error;
        }
    }

error:
    if (srv_fd)
        close(srv_fd);
    unlink(srv_addr);
    free(srv_addr);
}


// CLI

// 0 success
// -1 srv name error
// -2 in error
// -3 out error
// -4 net error (fuck)
int client_connect(const char * srv_name, int * srv_to_cli, int * cli_to_srv) {
    // if something goes wrong we could have some mem leak
    // do not care: if something goes wrong we are next to die,
    // and the OS will clean up

    // 0. Connect to master pipe 
    char * srv_addr = calloc((
                strlen(LOC) + 
                strlen(srv_name) +
                strlen(EXT) +
                1), sizeof(char));
    sprintf(srv_addr, "%s%s%s", LOC, srv_name, EXT);

    int master;

    if (!(master = open(srv_addr, O_RDWR))) {
        fprintf(logger, "[EE] connecting to srv %s\n", srv_addr);
        free(srv_addr);
        return -1;
    } else {
        fprintf(logger, "[II] connected to srv %s\n", srv_addr);
    }
    free(srv_addr);

    // srv -> client
    pid_t pid = getpid();
    char * srv_to_cli_addr = name_srv_to_cli(pid);

    if (mkfifo(srv_to_cli_addr, 0666) == -1) {
        fprintf(logger, "[EE] creating srv->cli fifo %s\n", srv_to_cli_addr);
        free(srv_to_cli_addr);
        return -2;
    } else {
        fprintf(logger, "[II] creted srv->cli fifo %s\n", srv_to_cli_addr);
    }

    if (!(*srv_to_cli = open(srv_to_cli_addr, O_RDWR))) {
        fprintf(logger, "[EE] opening srv->cli fifo\n");
        free(srv_to_cli_addr);
        return -2;
    } else {
        fprintf(logger, "[II] opened srv->cli fifo\n");
    }
    free(srv_to_cli_addr);

    // cli -> srv
    char * cli_to_srv_addr = name_cli_to_srv(srv_name, pid);
    if (mkfifo(cli_to_srv_addr, 0666) == -1) {
        fprintf(logger, "[EE] creating cli->srv fifo %s\n", cli_to_srv_addr);
        free(cli_to_srv_addr);
        return -3;
    } else {
        fprintf(logger, "[II] creted cli->srv fifo %s\n", cli_to_srv_addr);
    }

    if (!(*cli_to_srv = open(cli_to_srv_addr, O_RDWR))) {
        fprintf(logger, "[EE] opening cli->srv fifo %s\n", cli_to_srv_addr);
        free(cli_to_srv_addr);
        return -3;
    } else {
        fprintf(logger, "[II] opened cli->srv fifo %s\n", cli_to_srv_addr);
    }
    free(cli_to_srv_addr);

    // Init com - send pid
    if (write(master, (void *)&pid, sizeof(pid_t)) < 0) {
        fprintf(logger, "[EE] sending client pid %d\n", pid);
        return -4;
    } else {
        fprintf(logger, "[II] sent client pid %d\n", pid); 
    }
    
    {// Wait 4 server to be ready (200 OK)
        int status;
        if (read(*srv_to_cli, &status, sizeof(int)) < 0) {
            fprintf(logger, "[EE] receiving status\n");
            return -4;
        } else {
            fprintf(logger, "[II] received status %d\n", status);
        } 
    }

    return 0;
} 

// close connection and clean up
void client_close (const char * srv_name, const int srv_to_cli, const int cli_to_srv) {
    char * srv_to_cli_addr, * cli_to_srv_addr;

    if (srv_to_cli) {
        close(srv_to_cli);
        srv_to_cli_addr = name_srv_to_cli(getpid());
        unlink(srv_to_cli_addr);
        free(srv_to_cli_addr);
    }

    if (cli_to_srv) {
        close(cli_to_srv);
        cli_to_srv_addr = name_cli_to_srv(srv_name, getpid());
        unlink(cli_to_srv_addr);
        free(cli_to_srv_addr);
    }
}

// msg_len on success
// -1 reading error
// -2 error notified from other entity
// -3 mex_len excided
// COM_ERROR
int read_msg(int fd, char ** msg, const int max_len) {
    int len, com_error=0;

    if (read(fd, &len, sizeof(len)) < 0)
        return -1;

    if (max_len && len > max_len) {
        return -3;
    }

    // Error occurred on latest com [see rsv_core.c:ERORR]
    if ((com_error = (len == COM_ERROR)))
        if (read(fd, &len, sizeof(len)) < 0)
            return -1;

    (*msg) = (char*)malloc(sizeof(char) * (len+1));
    (*msg)[len] = '\0';

    if (read(fd, (*msg), sizeof(char)*len) < 0)
        return -1;
    return !com_error ? len : COM_ERROR;
}

// 1 on success
// 0 on error
int write_msg(int fd, const char * msg, int len) {
    // if not yet calculated: calculate
    len = !len ? strlen(msg) : len;
    return (write(fd, &len, sizeof(int)) >= 0) &&
        (write(fd, msg, sizeof(char) * len) >= 0);

}

void write_error(int fd, const char * msg) {
    if (write(fd, &COM_ERROR, sizeof(COM_ERROR)) == -1)
        fprintf(logger, "[EE] sending error message header\n"); 

    if (!write_msg(fd, msg, strlen(msg)))  
        fprintf(logger, "[EE] sending error message\n"); 
}

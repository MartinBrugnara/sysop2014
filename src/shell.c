/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#include <stdio.h>
#include <stdlib.h> // malloc
#include <string.h> // strncmp
#include <fcntl.h> // O_RDONLY
#include "mem.h" // print_list
#include "utils.h" // read_msg_EOF
#include "global.h" // CONF
#include "cad.h" // encode decode

void line(const char c) {
    int i=0;
    for (;i<80;i++) printf("%c", c);
    printf("\n");
}

void help() {
    line('=');
    printf("* \tHELP\n");
    printf("HELP - display this help message\n");
    printf("LIST - list encoded messages\n");
    printf("ENCO - encode message\n");
    printf("DECO - decode message\n");
    printf("QUIT - quit\n");
    printf("\n");
}

void list() {
   line('-'); 
   print_list();
}

static char *key=NULL;
void doit(int is_enco) {
    // init key buff if is not yet
    if (!key) key = malloc(sizeof(char) * CONF->max_key_len);
    
    // read key
    printf("Insert key [terminate on enter]: ");
    key[0] = '\n';
    int key_len = getline(&key, (size_t *)&(CONF->max_key_len), stdin); 
    if (key[key_len-1] == '\n')
        key[--key_len] = '\0'; //clean up leading \n

    if (key_len < CONF->min_key_len || key_len > CONF->max_key_len) {
        fprintf(logger, "[EE] Invalid key length [%d, %d]: %d\n>%s\n",
                CONF->min_key_len,
                CONF->max_key_len,
                key_len,
                key
                );
        return;
    }
    printf("\n");

    // read msg
    char * msg;
    int msg_len;
    if (is_enco) {
        printf("Write message [terminate on enter]: ");

        int msg_len_ext = CONF->max_mex_len + 1;

        msg = malloc(sizeof(char)*msg_len_ext);
        msg_len = getline(&msg, (size_t *)&msg_len_ext, stdin);
        if (msg[msg_len - 1] == '\n')
            msg[--msg_len] = '\0'; //clean up leading \n
    } else {
        printf("Write message id [terminate on enter]: ");
        int msg_id;
        scanf("%d", &msg_id);
        if (get(msg_id, &msg)) {
            printf("Invalid message_id\n");
            return;
        }
        msg_len = strlen(msg);
    }

    if (!msg_len) {
        fprintf(logger, "[EE] Empty message\n");
        return;
    } else if (msg_len > CONF->max_mex_len) {
        fprintf(logger, "[EE] Message too long\n");
        return;
    }

    char * res = is_enco ? encode(msg, key) : decode(msg, key);
    printf("Res: %s\n", res);
}

void enco() {
    doit(1);
}

void deco() {
    doit(0);
}

void quit () {
    //TODO: clean up before go out
    exit(0);
}

void shell() {
    help();
    char * cmd = malloc(sizeof(char)*5);
    int cmd_len_do_not_care;
    const int cmd_len = 4 + 1;
    while (1) {
        cmd_len_do_not_care = getline(&cmd, (size_t *)&cmd_len, stdin);
        cmd[4] = '\0';
        if (cmd_len_do_not_care != 5)
            continue;

        if (!strncmp(cmd, "HELP", 4)) {
            help();
        } else if (!strncmp(cmd, "LIST", 4)) {
            list();         
        } else if (!strncmp(cmd, "ENCO", 4)) {
            enco();
        } else if (!strncmp(cmd, "DECO", 4)) {
            deco();
        } else if (!strncmp(cmd, "QUIT", 4)) {
            quit();
        } else {
            fprintf(logger, "[EE] Invalid command: %s\n", cmd);
            help();
        }
    }    
}

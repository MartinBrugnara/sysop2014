/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#include "cad.h"

#include <string.h> /* strlen */
#include <stdlib.h> /* malloc */
#include "mem.h" /* store */

// alphabet size
const int MAGIC = 26; 

// alphabet boundary
const int A = (int)'A';
const int Z = (int)'Z';
const int a = (int)'a';
const int z = (int)'z';

// alphabet shift
const int tl = (int)'a' - (int) 'A';

/* support func */

// int: return letter absolute position in alphabet (0-26)
static inline 
int normalize(const int c_i) {
    return c_i < a ? A : a;
}

// boolean: verify if a char is a letter
static inline
int ck_boundary(const int c_i) {
    return ((c_i >= a && c_i <= z) || (c_i >= A && c_i <= Z));
}

// uppercase a string
void to_upper(char * s, const int len) {
    int i=0;
    for (;i<len;i++)
        if (s[i] >= a)
            s[i] -= tl;
}

/* main logic */
char *
translitter (const char * str, const char * key, const int is_enc) {
    int msg_len = strlen(str);
    int key_len = strlen(key);

    to_upper((char *)key, key_len);

    char * ret = (char*)malloc(sizeof(char) * (msg_len + 1));

    // iterator index
    int si=0, ki=0;

    // char cache
    char s_si;
    int s_si_i, key_ki_i;
    int ret_c_i, offset;

    for (;si < msg_len; si++) {

        s_si = str[si]; // char repr
        s_si_i = (int)s_si; // int repr

        if (!ck_boundary(s_si_i)) {
            ret[si] = s_si;
            continue;
        }
        
        // get char_idx in alphabet [0..25]
        ret_c_i = s_si_i - normalize(s_si_i);

        // get current translitter offset (key) [1..26]
        key_ki_i = key[ki];
        offset = key_ki_i - A + 1;

        // base aphabet + (translitter offset)          
        ret[si] = (char)(s_si_i - ret_c_i + 
                ((MAGIC + ret_c_i + (is_enc ? offset : -offset)) % MAGIC));

        ki = (ki + 1) % key_len;
    }
    return ret;
} 

/* exported func */
char *
encode (const char * msg, const char * key) {
    char * res = translitter(msg, key, 1);
    store(res);
    return res;
}

char * 
decode (const char * encoded_msg, const char * key) {
    return translitter(encoded_msg, key, 0);
}

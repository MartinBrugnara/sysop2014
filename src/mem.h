/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#ifndef MEM_H
#define MEM_H

//return -1 if store failed, 0 otherwise
int store(const char * msg);

//return 0 if msg founded, -1 otherwise
//put in *str the message
//required position from first elements (first= 0)
int get( const int i, char **str );

//print list
void print_list( void );

#endif

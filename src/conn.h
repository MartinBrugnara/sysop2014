/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#ifndef CONN_H
#define CONN_H

#include <unistd.h> //pid_t

// CMD
typedef enum{
    ENC, 
    DEC,
} cmd_t;


// SRV
// server_name: String, handler: worker
void server_up(const char *, void (* handler)(int, int, pid_t));

// CLI
// server_name :String, srv->cli, cli->srv :*FIFO
int client_connect(const char *, int *, int *);

// server_name :String, srv->cli, cli->srv :FIFO
void client_close (const char *, const int, const int);

// SHARED
int read_msg(const int fd, char ** msg, const int max_len);
int write_msg(const int fd, const char * msg, const int len);
void write_error(const int fd, const char * msg);
#endif 

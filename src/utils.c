/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "utils.h"

int intlen(const int num) {
    return floor(log10(abs(num))) + 1;
}

static inline
int max(const int a, const int b) {
    return a > b ? a : b;
}

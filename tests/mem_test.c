/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../src/mem.h"

int main(void){
    FILE * f = fopen("/dev/urandom", "r");

    const size_t len = 25;
    char * s = malloc(sizeof(char) * len+1);

    s[len] = '\0';

    int i, j;
    printf("Store values.\n");
    for (i=0; i<10; i++) {
        fread(s, sizeof(char), len, f);

        // normalize char
        for (j=0; j<len; j++)
            s[j] = (char)((int)s[j]%26)+'A'-1;

        printf("store: %s\n", s);

        store(s);
    }

    print_list();

    free(s);

    printf("Get values.\n");
    
    srand(time(0));
    int rid = rand()%10;

    if (!get(rid, &s))
        printf("SUCESS\n");
    else
        printf("FAILED\n");

    return 0;
}

/*
/ Martin Brugnara M:157791, Alex De Biasio M:158795
/ Client-Server CODER - Progetto 1
/ Sysop project 2014
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../src/cad.h"
#include "../src/mem.h"

int main (void){
    printf("Testing cad.h module\n");

    FILE * in;
    if (!(in = fopen("assets/cad_t.txt", "r"))) {
        fprintf(stderr, "File not found: assets/cad_t.txt\n"); 
        return 1;
    }

    int max_msg_len, max_key_len, n_tests;
    char * msg, * enc, * key; 

    fscanf(in, "%d %d %d", &max_msg_len, &max_key_len, &n_tests);

    msg = (char*) malloc(sizeof(char) * max_msg_len);
    enc = (char*) malloc(sizeof(char) * max_msg_len);
    key = (char*) malloc(sizeof(char) * max_key_len);

	char * enc_msg, * dec_msg;

    int t = 0;
    int p = 0;
    for (; t < n_tests; t++) {

        // " %[^\n]" -> it means:
        // ' ': skip all space, \n, \t etc..
        // %[^\n]: read until you meet '\n', then trash that '\n'

        fscanf(in, " %[^\n]\n", msg); 
        fscanf(in, " %[^\n]\n", enc); 
        fscanf(in, "%s", key); 

        printf("Testing %d:\n\t%s\n\t%s\n", t, msg, key);

        enc_msg = encode(msg, key);

        if (strcmp(enc, enc_msg)) {
            fprintf(stderr, "\tFailed! encoding error:\n\t%s\n", enc_msg);
            continue;
        }

        dec_msg = decode(enc_msg, key);
        if (strcmp(msg, dec_msg)) {
            fprintf(stderr, "\tFailed! decoding error\n");
            continue;
        }
        
        char * stored;
        if (get(0, &stored) || strcmp(enc_msg, stored)) {
            fprintf(stderr, "\tFailed! storing\n");
            continue;
        }
        fprintf(stdout, "Passed\n");
        p++;
    }

    fprintf(stdout, "\nCAD: passed %d/%d\n", p, t);

	return 0;
}

#!/bin/bash

# Martin Brugnara M:157791, Alex De Biasio M:158795
# Client-Server CODER - Progetto 1
# Sysop project 2014

# we log in this way because the process will be killed via SIGKILL
# and the log file will not be flushed / closed
nohup ./bin/server -n test -m 1024 -s 1 -l 128 >> /tmp/server.log &

echo "Waiting for server start up"
sleep 1

echo "Test 0 errors [if any]:"
./bin/client -n test -k dfgf -m AbBa -e -v /tmp/client.log -o enc.txt
cat /tmp/client.log | grep EE 

rm -f /tmp/client.log enc.txt

echo "Test 1 errors [if any]:"
./bin/client -n test -k Ciao -m "Test del messag%^(" -e -v /tmp/client.log -o enc.txt
cat /tmp/client.log | grep EE 

rm -f /tmp/client.log enc.txt

echo "Test completed.."
echo "Killing the server"

killall -9 server  > /dev/null 2>&1

rm -f /tmp/server.log
